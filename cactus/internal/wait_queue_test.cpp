#include <cactus/test.h>

#include <cactus/cactus.h>

#include <folly/ScopeGuard.h>

using namespace cactus;

FIBER_TEST_CASE("Wait queue cancelation") {
    WaitQueue wq;

    ServerGroup g;
    bool woken_up = false;

    {
        ServerGroup canceled_g;
        canceled_g.Spawn([&] {
            auto finally = folly::makeGuard([&] { wq.NotifyOne(); });

            wq.Wait();
        });

        canceled_g.Spawn([&] { wq.Wait(); });

        g.Spawn([&] {
            wq.Wait();
            woken_up = true;
        });

        SleepFor(std::chrono::milliseconds(1));
    }

    SleepFor(std::chrono::milliseconds(1));

    REQUIRE(woken_up);
}