add_subdirectory(gen-rpc)

add_library(cactus_rpc rpc.cpp)

add_proto_library(cactus_rpc_proto rpc.proto)

target_link_libraries(cactus_rpc cactus cactus_rpc_proto CONAN_PKG::protobuf)

add_proto_library(cactus_rpc_test_proto rpc_test.proto)

add_catch(test_cactus_rpc rpc_test.cpp)

target_link_libraries(test_cactus_rpc cactus_rpc_test_proto cactus_rpc)

add_catch(test_cactus_rpc_gen rpc_gen_test.cpp)

target_link_libraries(test_cactus_rpc_gen cactus_rpc_test_proto cactus_rpc)