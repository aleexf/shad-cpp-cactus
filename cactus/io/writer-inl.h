namespace cactus {

template <class... TArgs>
void IBufferedWriter::Format(const char* format, const TArgs&... args) {
    struct OutIterator : std::iterator<std::output_iterator_tag, char> {
    public:
        OutIterator(IBufferedWriter* writer, MutableView* buf) : writer_(writer), buf_(buf) {
        }

        OutIterator operator*() {
            return *this;
        }

        OutIterator operator++(int) {
            return *this;
        }

        OutIterator& operator++() {
            return *this;
        }

        OutIterator& operator=(char c) {
            if (buf_->size() == 0) {
                *buf_ = writer_->WriteNext();
            }

            *reinterpret_cast<char*>(buf_->data()) = c;
            *buf_ += 1;
            return *this;
        }

    private:
        IBufferedWriter* writer_;
        MutableView* buf_;
    };

    MutableView buf;
    fmt::format_to(OutIterator{this, &buf}, format, args...);
    if (buf.size() != 0) {
        WriteBackUp(buf.size());
    }
}

}  // namespace cactus