[build_requires]
protobuf/3.19.2

[requires]
folly/2022.01.31.00
catch2/2.13.8
benchmark/1.6.0
protobuf/3.19.2
fmt/8.1.1
glog/0.5.0
gflags/2.2.2
openssl/1.1.1n

[generators]
cmake
