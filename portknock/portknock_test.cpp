#include <cactus/test.h>

#include <cactus/cactus.h>

#include "portknock.h"

const folly::SocketAddress kLocalhost("127.0.0.1", 0);
constexpr int startPort = 25000;
constexpr int endPort = 26000;

FIBER_TEST_CASE("Knock TCP ports") {
    std::map<int, int> knocked;

    cactus::ServerGroup g;
    for (int i = startPort; i < endPort; i++) {
        g.Spawn([&, i] {
            auto addr = kLocalhost;
            addr.setPort(i);
            auto lsn = cactus::ListenTCP(addr);
            while(true) {
                lsn->Accept();
                knocked[i]++;
            }
        });
    }

    cactus::Yield();

    KnockPorts(kLocalhost, {{25001, KnockProtocol::TCP}, {25099, KnockProtocol::TCP}, {25999, KnockProtocol::TCP}, {25001, KnockProtocol::TCP}}, std::chrono::milliseconds(1));

    cactus::Yield();

    REQUIRE(knocked.size() == 3);
    REQUIRE(knocked[25001] == 2);
    REQUIRE(knocked[25099] == 1);
    REQUIRE(knocked[25999] == 1);
}